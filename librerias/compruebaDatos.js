
// Función que comprueba si hay archivos en el directorio datos, para no repetir los nombres, e ir guardando ordenadamente
// De no haber, comienza la numeración desde el 1, de haber, continúa con la nuemración desde último archivo
// Adicionalmente, también se encarga de obtener el mínimo, máximo, y de calcular el promedio
let compruebaDatos = () =>{
    const carpeta = './datos';
    const fs = require('fs');
    var i=0;
    var indicadores = {
        minDolar:[],minEuro:[],minTasa:[],
        maxDolar:[],maxEuro:[],maxTasa:[],
        promDolar:0,promEuro:0,promTasa:0,
        fechasDolar:[],fechasEuro:[],fechasTasa:[],
        actDolar:0,actEuro:0,actTasa:0
    };
    var dolar=[];
    var euro=[];
    var tasa=[];
   
    var acumDolar=0;
    var acumEuro=0;
    var acumTasa=0;
    

    return new Promise((resolve, reject) => {
        fs.readdirSync(carpeta).forEach(file => { //Se aprovecha el fs.readdirSync, y el forEach para contar los archivos, y obtener los datos mencionados al principio
            i++;
            const path = require("path");
            let datosRaw = fs.readFileSync(path.resolve(__dirname, `../datos/${file}`));
            let datos = JSON.parse(datosRaw);

            
            //Agrega los valores al arreglo para obtener mín y máx
            dolar.push(datos.dolar.valor);
            euro.push(datos.euro.valor);
            tasa.push(datos.tasa_desempleo.valor);
        
            //Agrega los valores al arreglo para calcular el promedio
            acumDolar=+datos.dolar.valor;
            acumEuro=+datos.euro.valor;
            acumTasa=+datos.tasa_desempleo.valor;
            
        });

        //Ordenando los arreglos
        dolar.sort((a, b) => a - b );
        euro.sort((a, b) => a - b );
        tasa.sort((a, b) => a - b );

        //Asignando los valores al objeto indicadores
        indicadores.minDolar[0]=dolar[0];
        indicadores.maxDolar[0]=dolar[(i-1)];
        indicadores.promDolar=(acumDolar/i);
        indicadores.minEuro[0]=euro[0];
        indicadores.maxEuro[0]=euro[(i-1)];
        indicadores.promEuro=(acumEuro/i);
        indicadores.minTasa[0]=tasa[0];
        indicadores.maxTasa[0]=tasa[(i-1)];
        indicadores.promTasa=(acumTasa/i);

        //Asignando rango de fechas de archivos consultados, más antigua y más actual(de manera muy poco elegante)
        // Se usa esta misma parte de código para asignar los valores más actuales
        const path = require("path");  
        let fecha1 = JSON.parse(fs.readFileSync(path.resolve(__dirname, `../datos/1indicadores.ind`)));
        let fecha2 = JSON.parse(fs.readFileSync(path.resolve(__dirname, `../datos/${i}indicadores.ind`)));
        indicadores.fechasDolar.push(fecha1.dolar.fecha)
        indicadores.fechasDolar.push(fecha2.dolar.fecha)
        indicadores.actDolar= fecha2.dolar.valor
        indicadores.fechasEuro.push(fecha1.euro.fecha)
        indicadores.fechasEuro.push(fecha2.euro.fecha)
        indicadores.actEuro= fecha2.euro.valor
        indicadores.fechasTasa.push(fecha1.tasa_desempleo.fecha)
        indicadores.fechasTasa.push(fecha2.tasa_desempleo.fecha)
        indicadores.actTasa= fecha2.tasa_desempleo.valor

        if(i===0){        
            reject ((i+1));
        }else{     
            resolve ({i, indicadores});       
        }
    })
    
}

module.exports = compruebaDatos;