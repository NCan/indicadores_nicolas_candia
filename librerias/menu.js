function menu(lector, callback){
    console.log ('\n MENU');
    console.log ('1.- Actualizar indicadores (Escribe nuevo archivo)');
    console.log ('2.- Promediar');
    console.log ('3.- Mostrar valor más actual');
    console.log ('4.- Mostrar mínimo histórico');
    console.log ('5.- Mostrar máximo histórico');
    console.log ('6.- Salir');
    lector.question('Escriba su opción: ', opcion => {
        callback(opcion)
    });
}

module.exports = menu;