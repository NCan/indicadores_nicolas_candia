var compruebaDatos= require('./compruebaDatos')


function obtenerIndicadores(){
    const fs= require('fs');
    const fetch= require('node-fetch');
    let numero;

    const status = response =>{
        if(response.status >= 200 && response.status < 300){
            return Promise.resolve(response);
        }
        return Promise.reject(console.log("Consumo de servicio fallido"));
    };

    compruebaDatos().then( hay =>{
        numero=(hay.i)+1;
    }, (noHay) =>{
        numero=noHay;
    });

    fetch('https://mindicador.cl/api')
    .then(res => res.json())
    .then(json =>fs.writeFileSync(`./datos/${numero}indicadores.ind`,JSON.stringify(json, null, 2) ))

}
module.exports = obtenerIndicadores;