var obtenerIndicadores = require('./librerias/obtenerIndicadores.js')
var menuFuncion= require('./librerias/menu')
var compruebaDatos= require('./librerias/compruebaDatos')

const readline = require('readline');



var lector= readline.createInterface({
    input:  process.stdin,
    output: process.stdout
});




function menuCallback(opcion){
        switch(opcion){
            case '1':
                obtenerIndicadores();
                menuFuncion(lector, menuCallback);
                break;
            case '2':
                    console.log("\n1- Dolar");
                    console.log("2- Euro");
                    console.log("3- Tasa de desempleo");
            
                lector.question("Escoja su opción: ", opcion =>{
     
                switch(opcion){
                    case '1':
                            compruebaDatos().then( hay =>{
                                console.log("|-------------------------------DOLAR------------------------------------------------------|");
                                console.log(" El Rango de fechas para el Dolar es: "+ hay.indicadores.fechasDolar[0]+" a "+ hay.indicadores.fechasDolar[1]);
                                console.log(" EL promedio histórico del Dolar es   : $ "+ hay.indicadores.promDolar);
                                console.log("|------------------------------------------------------------------------------------------|");
                                menuFuncion(lector, menuCallback);
                            }, noHay =>{
                                console.log("\nNo existen archivos para realizar la acción solicitada");
                                menuFuncion(lector, menuCallback);
                            });
                        break;
                    case '2':
                            compruebaDatos().then( hay =>{
                                console.log("\n|-------------------------------EURO-------------------------------------------------------|");
                                console.log(" El Rango de fechas para el Euro es: "+ hay.indicadores.fechasEuro[0]+" a "+ hay.indicadores.fechasEuro[1]);
                                console.log(" EL promedio histórico del Euro es: $ "+ hay.indicadores.promEuro);
                                console.log("|------------------------------------------------------------------------------------------|");
                                menuFuncion(lector, menuCallback);
                            }, noHay =>{
                                console.log("\nNo existen archivos para realizar la acción solicitada");
                                menuFuncion(lector, menuCallback);
                            });
                        break;
                    case '3':
                            compruebaDatos().then( hay =>{
                                console.log("\n|-------------------------------TASA DE DESEMPLEO------------------------------------------|");
                                console.log(" El Rango de fechas para la tasa de desempleo es: "+ hay.indicadores.fechasTasa[0]+" a "+ hay.indicadores.fechasTasa[1]);
                                console.log(" EL promedio histórico de la tasa de desempleo es: $ "+ hay.indicadores.promTasa);
                                console.log("|------------------------------------------------------------------------------------------|");
                                menuFuncion(lector, menuCallback);
                            }, noHay =>{
                                console.log("\nNo existen archivos para realizar la acción solicitada");
                                menuFuncion(lector, menuCallback);
                            });
                        break;
                        default:
                                console.log('opción no encontrada'); 
                                menuFuncion(lector, menuCallback);
                                break;    
                   

                }

            });
 


 
            break;
            case '3':
                    compruebaDatos().then( hay =>{
                        console.log("\n|-------------------------------DOLAR------------------------------------------------------|");
                        console.log(" La fecha más actual para el Dolar es: "+ hay.indicadores.fechasDolar[1]);
                        console.log(" Su precio es                        : $ "+ hay.indicadores.actDolar);
                        console.log("|------------------------------------------------------------------------------------------|");
                        
                        console.log("\n|-------------------------------EURO-------------------------------------------------------|");
                        console.log(" La fecha más actual para el Euro es: "+ hay.indicadores.fechasEuro[1]);
                        console.log(" Su precio es                       : $ "+ hay.indicadores.actEuro);
                        console.log("|------------------------------------------------------------------------------------------|");
    
                        console.log("\n|-------------------------------TASA DE DESEMPLEO------------------------------------------|");
                        console.log(" La fecha más actual para la tasa de desempleo es: "+ hay.indicadores.fechasTasa[1]);
                        console.log(" Su precio es                                    : $ "+ hay.indicadores.actTasa);
                        console.log("|------------------------------------------------------------------------------------------|");
                        menuFuncion(lector, menuCallback);
                    }, noHay =>{
                        console.log("\nNo existen archivos para realizar la acción solicitada");
                        menuFuncion(lector, menuCallback);
                    });
                    break;
            case '4':
                compruebaDatos().then( hay =>{
                    console.log("\n|-------------------------------DOLAR------------------------------------------------------|");
                    console.log(" El Rango de fechas para el Dolar es: "+ hay.indicadores.fechasDolar[0]+" a "+ hay.indicadores.fechasDolar[1]);
                    console.log(" EL mínimo histórico del Dolar es   : $ "+ hay.indicadores.minDolar[0]);
                    console.log("|------------------------------------------------------------------------------------------|");
                    
                    console.log("\n|-------------------------------EURO-------------------------------------------------------|");
                    console.log(" El Rango de fechas para el Euro es: "+ hay.indicadores.fechasEuro[0]+" a "+ hay.indicadores.fechasEuro[1]);
                    console.log(" EL mínimo histórico del Euro es: $ "+ hay.indicadores.minEuro);
                    console.log("|------------------------------------------------------------------------------------------|");

                    console.log("\n|-------------------------------TASA DE DESEMPLEO------------------------------------------|");
                    console.log(" El Rango de fechas para la tasa de desempleo es: "+ hay.indicadores.fechasTasa[0]+" a "+ hay.indicadores.fechasTasa[1]);
                    console.log(" EL mínimo histórico de la tasa de desempleo es: $ "+ hay.indicadores.minTasa);
                    console.log("|------------------------------------------------------------------------------------------|");
                    menuFuncion(lector, menuCallback);
                }, noHay =>{
                    console.log("\nNo existen archivos para realizar la acción solicitada");
                    menuFuncion(lector, menuCallback);
                });
                break;
            case '5':
                compruebaDatos().then( hay =>{
                    console.log("\n|-------------------------------DOLAR------------------------------------------------------|");
                    console.log(" El Rango de fechas para el Dolar es: "+ hay.indicadores.fechasDolar[0]+" a "+ hay.indicadores.fechasDolar[1]);
                    console.log(" El máximo histórico del Dolar es   : $ "+ hay.indicadores.maxDolar[0]);
                    console.log("|------------------------------------------------------------------------------------------|");
                    
                    console.log("\n|-------------------------------EURO-------------------------------------------------------|");
                    console.log(" El Rango de fechas para el Euro es: "+ hay.indicadores.fechasEuro[0]+" a "+ hay.indicadores.fechasEuro[1]);
                    console.log(" El máximo histórico del Euro es: $ "+ hay.indicadores.maxEuro);
                    console.log("|------------------------------------------------------------------------------------------|");

                    console.log("\n|-------------------------------TASA DE DESEMPLEO------------------------------------------|");
                    console.log(" El Rango de fechas para la tasa de desempleo es: "+ hay.indicadores.fechasTasa[0]+" a "+ hay.indicadores.fechasTasa[1]);
                    console.log(" El máximo histórico de la tasa de desempleo es: $ "+ hay.indicadores.maxTasa);
                    console.log("|------------------------------------------------------------------------------------------|");
                    menuFuncion(lector, menuCallback);
                }, noHay =>{
                    console.log("\nNo existen archivos para realizar la acción solicitada");
                    menuFuncion(lector, menuCallback);
                });
                break;
            case '6':
                console.log('Adios!');
                lector.close();
                process.exit(0);
                break; 
               
            default:
                console.log('opción no encontrada'); 
                break;       
        }
}


menuFuncion(lector, menuCallback);

